package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private  Balloon balloon;
    private  JButton moveButton;
    private Timer timer;
    private List<Balloon> lotsOfBalloons;
    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);

        this.moveButton = new JButton("Move balloon");
        this.moveButton.addActionListener(this);
        this.add(moveButton);

        this.addKeyListener(this);

        this.timer = new Timer(200, this);

        this.lotsOfBalloons =  new ArrayList<Balloon>();
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        balloon.move();
        for(Balloon balloon : lotsOfBalloons){
            balloon.move();
        }

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);
        for(Balloon balloon : lotsOfBalloons){
            balloon.draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        timer.stop();
    int direction = e.getKeyCode();
    switch (direction){
        case KeyEvent.VK_UP:
            timer.start();
            balloon.setDirection(Direction.Up);
            break;
        case KeyEvent.VK_DOWN:
            timer.start();
            balloon.setDirection(Direction.Down);
            break;
        case KeyEvent.VK_LEFT:
            timer.start();
            balloon.setDirection(Direction.Left);
            break;
        case KeyEvent.VK_RIGHT:
            timer.start();
            balloon.setDirection(Direction.Right);
            break;
            case KeyEvent.VK_S:
                timer.stop();
                break;
        case KeyEvent.VK_A:
            lotsOfBalloons.add(new Balloon ((int)(Math.random()*800),(int)(Math.random()*800)));
            timer.start();
            break;

    }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}