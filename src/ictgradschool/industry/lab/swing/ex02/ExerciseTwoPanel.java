package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {
    private JButton add;
    private JButton subtract;
    private JTextField userAmount1;
    private JTextField userAmount2;
    private JTextField result;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        userAmount1 = new JTextField(10);
        this.add(userAmount1);

        userAmount2 = new JTextField(10);
        this.add(userAmount2);

        add = new JButton("Add");
        this.add(add);
        add.addActionListener(this);

        subtract = new JButton("Subtract");
        this.add(subtract);
        subtract.addActionListener(this);

        JLabel resultField = new JLabel("Results: ");
        this.add(resultField);
        result = new JTextField(15);
        this.add(result);


    }
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == add){
            double userInput1 = Double.parseDouble(userAmount1.getText());
            double userInput2 = Double.parseDouble(userAmount2.getText());
            double results = roundTo2DecimalPlaces(userInput1+userInput2);
            result.setText(Double.toString(results));
        }else if (event.getSource() == subtract){
            double userInput1 = Double.parseDouble(userAmount1.getText());
            double userInput2 = Double.parseDouble(userAmount2.getText());
            double results = roundTo2DecimalPlaces(userInput1-userInput2);
            result.setText(Double.toString(results));
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}